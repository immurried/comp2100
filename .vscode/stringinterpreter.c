
#include <stdio.h>
#include <string.h>
#include <math.h>

void PrintStringAsDec(char* c);
void PrintStringAsOct(char* c);
void PrintStringAsHex(char* c);

int main (int argc, char** argv)
{
    char unit[] = "COMP2100";

    PrintStringAsDec(unit);
    PrintStringAsOct(unit);
    PrintStringAsHex(unit);

}

void PrintStringAsDec(char* c)
{
    for (int i = 0; i < strlen(c); i++)
    {
        printf ("%d ", c[i]);
    }
    printf("\n");
}

void PrintStringAsOct(char* c)
{
    for (int i = 0; i < strlen(c); i++)
    {
        printf ("%o ", c[i]);
    }
    printf("\n");
}

void PrintStringAsHex(char* c)
{
    for (int i = 0; i < strlen(c); i++)
    {
        printf ("%x ", c[i]);
    }
    printf("\n");
}