/*
	Author: Zachary Dingwall
*/


#include <stdio.h>
#include <stdint.h>
#include <string.h>

struct record
{
	double seashore;
	char corn[8];
	float rifle;
	unsigned long steel;
	int32_t board;
	short window;
	unsigned int pail;
	char silver;
	int16_t toes;
	int8_t dust;
	int16_t gun;
	int32_t basketball;
	uint8_t spiders;
	unsigned short weather;
	int16_t frog;
	long picture;
	double observation;
};

void ParseRecord(struct record);

int main (int argc, char **argv)
{
	struct record r = {	
			0.002579,
			"desire",
			-96.25,
			18,
			1,
			890,
			372999210,
			'E',
			0,
			0,
			1,
			0,
			2,
			30,
			0,
			-577,
			-157126.113037
			};

	ParseRecord(r);

	return 0;
}

void ParseRecord(struct record r)
{
	printf("seashore, corn, rifle, steel, board, window, pail, silver, toes, dust, gun, basketball, spiders, weather, frog, picture, observation");
	printf("\n");
	printf("%lf, %s, %f, %lu, %d, %d, %u, %c, %d, %d, %d, %d, %u, %u, %d, %lx, %lf",
		r.seashore, r.corn, r.rifle, r.steel, r.board, r.window, r.pail, r.silver, r.toes, r.dust, r.gun, r.basketball, r.spiders, r.weather, r.frog, r.picture, r.observation);
}